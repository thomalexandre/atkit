# ATKit #

AtKit is a set of 2 frameworks for iOS and tvOS I built to make my life easier building iOS application.
Each time I create something new I try to put it there to share between my apps.

### Atk_Core ###

Core contains low level code: String manipulation, Date, APi call, Core data, Application settings..

### Atk_UI ###

UI contains set of Category for UI Kit classes, View controllers, Views...

### tvOS ###

The 2 frameworks have the equivalent for tvOS anc can be used as Framework there too.
* Atk_Core_tv
* Atk_UI_tv


### Usage ###

* [Stories](https://itunes.apple.com/us/app/best-current-stories-from/id1045610286) *Best Current Stories From The Internet*
* [Fun with betting](https://itunes.apple.com/us/app/fun-with-betting-euro-2016/id1090764993?ls=1&mt=8) *Bet with your friends for free for the soccer competitions*
* [LeanVocab](http://leanvocab.herokuapp.com/) *Practice your vocab everyday*