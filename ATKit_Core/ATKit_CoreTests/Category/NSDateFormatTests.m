//
//  NSDateFormatTests.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/5/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSDate+Format.h"

@interface NSDateFormatTests : XCTestCase

@property (strong, nonatomic) NSDate *date;

@end

@implementation NSDateFormatTests

- (void)setUp
{
    [super setUp];
    
    self.date = [[NSDate alloc] initWithTimeIntervalSince1970:1457166885]; // GMT: Sat, 05 Mar 2016 08:34:45 GMT
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testFormatWithFormat
{
    XCTAssert(nil == [self.date formatWithFormat:nil]);
    XCTAssert([@"2016-03-05" isEqualToString:[self.date formatWithFormat:@"yyyy-MM-dd"]]);
    XCTAssert([@"09:34:45" isEqualToString:[self.date formatWithFormat:@"HH:mm:ss"]]);
}

- (void)testUniversalFormat
{
    XCTAssert([@"2016-03-05T09:34:45Z" isEqualToString:[self.date universalFormat]]);
}

- (void)testDateWithString
{
    NSString *dateString    = @"2016-03-05T09:34:45";
    NSString *format        = @"yyyy-MM-dd'T'HH:mm:ss";
    NSDate *dateFromString  = [NSDate dateFromString:dateString withFormat:format];
    
    XCTAssert(self.date.timeIntervalSince1970 == dateFromString.timeIntervalSince1970);
}

- (void)testDateWithStringTimezone
{
    NSString *dateString    = @"2016-03-06T10:00:00";
    NSString *timezone      = @"Europe/London";
    NSString *format        = @"yyyy-MM-dd'T'HH:mm:ss";
    NSDate *dateFromString  = [NSDate dateFromString:dateString withTimezone:timezone withFormat:format];
    
    XCTAssert(dateFromString.timeIntervalSince1970 == 1457258400);
    XCTAssert([@"2016-03-06T11:00:00Z" isEqualToString:[dateFromString universalFormat]]);
}

- (void)testLocalWithDateFormat
{
    XCTAssert([@"Saturday, March 5, 2016 at 9:34:45 AM Central European Standard Time" isEqualToString:[self.date localWithDateFormat:NSDateFormatterFullStyle withTimeFormat:NSDateFormatterFullStyle]]);
    XCTAssert([@"Mar 5, 2016, 9:34:45 AM" isEqualToString:[self.date localWithDateFormat:NSDateFormatterMediumStyle withTimeFormat:NSDateFormatterMediumStyle]]);
    XCTAssert([@"3/5/16" isEqualToString:[self.date localWithDateFormat:NSDateFormatterShortStyle withTimeFormat:NSDateFormatterNoStyle]]);
}

- (void)testTimeStampToDate
{
    NSDate *date = [NSDate timestampToDate:@"1457728759"];
    NSString *dateString = [date universalFormat];
    XCTAssert([@"2016-03-11T21:39:19Z" isEqualToString:dateString]);
}


@end
