//
//  NSStringNumberTests.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/7/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+Number.h"

@interface NSStringNumberTests : XCTestCase

@end

@implementation NSStringNumberTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testAllDigits
{
    NSString *nilString = nil;
    XCTAssert(NO == [nilString isAllDigits]);
    XCTAssert(NO == [@"" isAllDigits]);
    XCTAssert(YES == [@"3435231" isAllDigits]);
    XCTAssert(NO == [@"343.5231" isAllDigits]);
    XCTAssert(YES == [@"0" isAllDigits]);
    XCTAssert(NO == [@"sdfnsjdnfjnfjsn" isAllDigits]);
}


@end
