//
//  NSDateCalculation.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/6/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSDate+Calculation.h"

@interface NSDateCalculation : XCTestCase

@end

@implementation NSDateCalculation

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testIsDue
{
    NSDate *someDate = [[NSDate alloc] initWithTimeIntervalSince1970:1425243768]; // Sun, 01 Mar 2015 21:02:48 GMT
    XCTAssert([someDate isDue] == YES);
    
    NSDate *nowMinus5Seconds = [[NSDate alloc] initWithTimeIntervalSince1970:[[NSDate new] timeIntervalSince1970] - 5];
    XCTAssert([nowMinus5Seconds isDue] == YES);
}

- (void)testIsNotDue
{
    NSDate *someDate = [[NSDate alloc] initWithTimeIntervalSince1970:2687547768]; // Mon, 01 Mar 2055 21:02:48 GMT
    XCTAssert([someDate isDue] == NO);
    
    NSDate *nowMinus5Seconds = [[NSDate alloc] initWithTimeIntervalSince1970:[[NSDate new] timeIntervalSince1970] + 5];
    XCTAssert([nowMinus5Seconds isDue] == NO);
}

- (void)testIsLaterThan
{
    NSDate *someDate = [[NSDate alloc] initWithTimeIntervalSince1970:2687547768]; // Mon, 01 Mar 2055 21:02:48 GMT
    NSDate *now = [NSDate new];
    XCTAssert(![now isLaterThan:someDate]);
    XCTAssert([someDate isLaterThan:now]);
}


@end
