//
//  NSDictionaryTests.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/5/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSDictionary+DefaultValue.h"

@interface NSDictionaryTests : XCTestCase

@property (strong, nonatomic) NSDictionary *dict;

@end

@implementation NSDictionaryTests

- (void)setUp
{
    [super setUp];
    
    self.dict = @{@"key1" : @"stringValue",
                  @"key2" : @100,
                  };
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testValueForKeyWithDefault
{
    XCTAssert([[self.dict valueForKey:@"key1" withDefault:@"stringValueDUMMY"] isEqualToString:@"stringValue"]);
    XCTAssert([[self.dict valueForKey:@"key2" withDefault:@5453534] isEqualToNumber:@100]);
    XCTAssert([[self.dict valueForKey:@"key3" withDefault:@"stringValueDUMMY"] isEqualToString:@"stringValueDUMMY"]);
    XCTAssert([[self.dict valueForKey:@"key4" withDefault:@5453534] isEqualToNumber:@5453534]);
}

- (void)testValueExistsForKey
{
    XCTAssert([self.dict valueExistsForKey:@"key1"]);
    XCTAssert([self.dict valueExistsForKey:@"key2"]);
    XCTAssert(![self.dict valueExistsForKey:@"key3"]);
    XCTAssert(![self.dict valueExistsForKey:nil]);
}

@end
