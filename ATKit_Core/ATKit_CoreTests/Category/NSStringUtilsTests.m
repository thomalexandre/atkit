//
//  NSStringUtilsTests.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/23/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+Utils.h"

@interface NSStringUtilsTests : XCTestCase

@end

@implementation NSStringUtilsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testEmpty
{
    XCTAssert(YES == [@"" empty]);
    XCTAssert(NO  == [@"Something inside" empty]);
}


@end
