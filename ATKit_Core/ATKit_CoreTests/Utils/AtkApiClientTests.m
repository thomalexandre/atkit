//
//  AtkApiClientTests.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/5/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AtkApiClient.h"
#import "NSData+Convert.h"

@interface AtkApiClientTests : XCTestCase

@property(nonatomic, strong) AtkApiClient *apiClient;

@end

@implementation AtkApiClientTests

- (void)setUp
{
    [super setUp];
    
    self.apiClient = [AtkApiClient new];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testEarthQuakes
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"earthquake tests"];
    
    NSURL *url = [NSURL URLWithString:@"http://earthquake-report.com/feeds/recent-eq?json"];
    
    [self.apiClient getRequestWithURL:url withHeader:nil withPayload:nil withCacheDuration:0 onSuccess:^(NSData *data) {
        NSArray *earthQuakes = [data jsonArray];
        XCTAssert(earthQuakes);
        XCTAssert([earthQuakes count] > 0);
        [expectation fulfill];
    } onFailure:^(NSError *error) {
        XCTAssert(NO);
        [expectation fulfill];
    }];

    [self waitForExpectationsWithTimeout:5.0 handler:nil];
}

- (void)testDummyForError
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"dummy server tests"];
    
    NSURL *url = [NSURL URLWithString:@"http://thisdummyserverdoesntexist.com/"];
    
    [self.apiClient getRequestWithURL:url withHeader:nil withPayload:nil withCacheDuration:0 onSuccess:^(NSData *data) {
        XCTAssert(NO);
        [expectation fulfill];
    } onFailure:^(NSError *error) {
        XCTAssert(error);
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:5.0 handler:nil];
}


@end
