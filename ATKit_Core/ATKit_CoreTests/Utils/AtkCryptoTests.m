//
//  AtkCryptoTest.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 2/19/16.
//  Copyright © 2016 athomas. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AtkCrypto.h"

@interface AtkCryptoTests : XCTestCase

@end

@implementation AtkCryptoTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testEncryptedPassword
{
    
    NSString *password = @"abcd";
    NSString *salt     = @"rqs52m46je8o8400sskk4scwggsc840";
    NSString *encryptedPassword = [AtkCrypto encryptPassword:password withSalt:salt];
    
    XCTAssert([@"smJ/F3L/fngn1Vn95PqPDguFkAYFM29DOWqU3XEmfOOfCg+CKOTpMLyv0x0m2KZY45NkGBsn0grouCeuf2tCbA==" isEqualToString:encryptedPassword]);
}

- (void)testAuthenticationHeader
{
    NSString *username = @"alex";
    NSString *password = @"abcd";
    NSString *salt     = @"rqs52m46je8o8400sskk4scwggsc840";
    NSString *encryptedPassword = [AtkCrypto encryptPassword:password withSalt:salt];
    NSString *header = [AtkCrypto authenticationHeaderWithUserName:username withEncryptedassword:encryptedPassword];
    
    //NSLog(@"%@", header);
}

- (void)testEncryptText
{
    NSString *text = @"https://www.google.hu/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png";
    NSString *encrypted = [AtkCrypto encryptText:text];
    XCTAssert([@"ygumkfAQkpBcXlmDOsnw0652S8g=" isEqualToString:encrypted]);
}

- (void)testAES128PKCS7Encrypt
{
     NSString *AESEncoded = [AtkCrypto AES128PKCS7EncryptString:@"testing encryption"
                                                        withKey:@"Xr62gDV3RFaWiHHc"
                                                         withIV:@"jQcYMW5i2OwLxtEw"];
    XCTAssert([@"96MLZfxDUTiRogHglRtDbt6TsDGEoMzWyq+FQDTYGyg=" isEqualToString:AESEncoded]);
    
}

@end
