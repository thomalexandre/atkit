//
//  NSData+Convert.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/5/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "NSData+Convert.h"

@implementation NSData (Convert)

- (id)json
{
    if (self.length) {
        NSError *jsonError;
        return [NSJSONSerialization JSONObjectWithData:self options:0 error:&jsonError];
    }
    
    return nil;
}

- (NSDictionary *)jsonDictionary
{
    id json = [self json];
    if ([json isKindOfClass:[NSDictionary class]]) {
        return (NSDictionary *)json;
    }

    return nil;
}

- (NSArray *)jsonArray
{
    id json = [self json];
    if ([json isKindOfClass:[NSArray class]]) {
        return (NSArray *)json;
    }
    
    return nil;
}

- (BOOL)isDictionary
{
    return [[self json] isKindOfClass:[NSDictionary class]];
}

- (BOOL)isArray
{
    return [[self json] isKindOfClass:[NSArray class]];
}

@end
