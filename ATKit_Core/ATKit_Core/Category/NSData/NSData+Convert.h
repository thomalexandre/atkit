//
//  NSData+Convert.h
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/5/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Convert)

// Convert the Data into a JSON object.
- (id)json;

/// Convert the Data into a JSON dictionary.
- (NSDictionary *)jsonDictionary;

/// Convert the Data into a JSON array.
- (NSArray *)jsonArray;

/// Returns YES if the data is a dictionary. NO otherwise.
- (BOOL)isDictionary;

/// Returns YES if the data is an array. NO otherwise.
- (BOOL)isArray;

@end
