//
//  NSDictionary+DefaultValue.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/4/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "NSDictionary+DefaultValue.h"

@implementation NSDictionary (DefaultValue)

- (id)valueForKey:(NSString *)key withDefault:(id)defaultValue
{
    id value = [self valueForKey:key];
    
    if (nil != value && value != [NSNull null]) {
        return value;
    }
    
    return defaultValue;
}

- (BOOL)valueExistsForKey:(NSString *)key
{
    return nil != [self valueForKey:key withDefault:nil];
}

@end
