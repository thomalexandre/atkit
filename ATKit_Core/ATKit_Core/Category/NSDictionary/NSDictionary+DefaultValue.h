//
//  NSDictionary+Value.h
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/4/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (DefaultValue)

/// Return the value for the key or the default value if cannot be found in the dictionary.
///
/// @param key          : the key to look for in the dictionary.
/// @param defaultValue : the default value to return in case there is no value associated to the key.
- (id)valueForKey:(NSString *)key withDefault:(id)defaultValue;

/// Returns true if the key exists in the dictrionary.
///
/// @param key : the key to look for in the dictionary.
- (BOOL)valueExistsForKey:(NSString *)key;

@end
