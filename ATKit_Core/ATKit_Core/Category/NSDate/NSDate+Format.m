//
//  NSDate+Format.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/4/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "NSDate+Format.h"

#define kUniversalFormat @"yyyy-MM-dd'T'HH:mm:ss'Z'"

@implementation NSDate (Format)

- (NSString *)formatWithFormat:(NSString *)format
{
    if(!format) {
        return nil;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:format];
    return [dateFormatter stringFromDate:self];
}

- (NSString *)universalFormat
{
    return [self formatWithFormat:kUniversalFormat];
}

+ (NSDate *)dateFromString:(NSString *)dateString withFormat:(NSString *)format
{
    return [NSDate dateFromString:dateString withTimezone:nil withFormat:format];
}

+ (NSDate *)dateFromString:(NSString *)dateString withTimezone:(NSString *)timezone withFormat:(NSString *)format
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if(timezone) {
        dateFormatter.timeZone = [NSTimeZone timeZoneWithName:timezone];
    }
    [dateFormatter setDateFormat:format];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:dateString];
    
    return dateFromString;
}

- (NSString *)localWithDateFormat:(NSDateFormatterStyle)dateFormat withTimeFormat:(NSDateFormatterStyle)timeFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = dateFormat;
    dateFormatter.timeStyle = timeFormat;
    [dateFormatter setLocale:[NSLocale currentLocale]];
    return [dateFormatter stringFromDate:self];
}

+ (NSDate *)timestampToDate:(NSString *)timestamp
{
    double timestampValue = [timestamp doubleValue];
    return [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)timestampValue];
}

- (NSString *)dateAgoAsEnglish
{
    int seconds = [[NSDate date] timeIntervalSinceDate:self];
    int forHours = seconds / 3600;
    int days = forHours / 24;
    int weeks = days / 7;
    int remainder = seconds % 3600;
    int forMinutes = remainder / 60;
    
    NSString *ago;
    if(weeks > 1)
        ago = [NSString stringWithFormat:@"%d weeks ago", weeks];
    else if(weeks == 1)
        ago = [NSString stringWithFormat:@"%d week ago", weeks];
    else if(days > 1)
        ago = [NSString stringWithFormat:@"%d days ago", days];
    else if(days == 1)
        ago = [NSString stringWithFormat:@"%d day ago", days];
    else if(forHours > 1)
        ago = [NSString stringWithFormat:@"%d hours ago", forHours];
    else if(forHours == 1)
        ago = [NSString stringWithFormat:@"%d hour ago", forHours];
    else if(forMinutes > 1)
        ago = [NSString stringWithFormat:@"%d minutes ago", forMinutes];
    else if(forMinutes == 1)
        ago = [NSString stringWithFormat:@"%d minute ago", forMinutes];
    
    return ago;
}

@end
