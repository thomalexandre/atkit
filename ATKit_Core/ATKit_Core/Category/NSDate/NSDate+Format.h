//
//  NSDate+Format.h
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/4/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Format)

/// Format the date with the given format.
///
/// @param format : the format to use to convert the date.
- (NSString *)formatWithFormat:(NSString *)format;

/// Returns the date using the universal format.
/// Used format : "yyyy-MM-dd'T'HH:mm:SS'Z'".
- (NSString *)universalFormat;

/// Returns the date stored in the given string using the passed format.
///
/// @param dateString : the formated date.
/// @param format     : the rormat to use for the conversion.
+ (NSDate *)dateFromString:(NSString *)dateString withFormat:(NSString *)format;

/// Returns the date from the assiciated timezone stored in the given string using the passed format.
///
/// @param dateString : the formated date.
/// @param timezone   : Timezone where the time is loca, eg: "Europe/Budapest"
/// @param format     : the rormat to use for the conversion.
+ (NSDate *)dateFromString:(NSString *)dateString withTimezone:(NSString *)timezone withFormat:(NSString *)format;

/// Format converter using date and time styles
///
/// @param dateFormat : Format of the date.
/// @param timeFormat : format of the time.
- (NSString *)localWithDateFormat:(NSDateFormatterStyle)dateFormat withTimeFormat:(NSDateFormatterStyle)timeFormat;

/// Convert the given timestamp into a Data object.
+ (NSDate *)timestampToDate:(NSString *)timestamp;

/// Compare the date with the current date and return a string with the following format
/// 5 weeks ago
/// 3 days ago
/// 1 hour ago
/// 10 minutes ago
- (NSString *)dateAgoAsEnglish;

@end
