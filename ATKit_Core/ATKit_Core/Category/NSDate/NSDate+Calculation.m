//
//  NSDate+Calculation.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/4/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "NSDate+Calculation.h"

@implementation NSDate (Calculation)

+ (NSDate *)addHoursToCurrentTime:(int)hoursToAdd
{
    return [[NSDate date] addHours:hoursToAdd];
}

- (NSDate *)addHours:(int)hoursToAdd
{
    return [self dateByAddingTimeInterval:60 * 60 * hoursToAdd];
}

+ (NSDate *)addDaysToCurrentTime:(int)daysToAdd
{
    return [[NSDate date] addDays:daysToAdd];
}

- (NSDate *)addDays:(int)daysToAdd
{
    return [self dateByAddingTimeInterval:60 * 60 * 24 * daysToAdd];
}

+ (NSDate *)addMinutessToCurrentTime:(int)minutesToAdd
{
    return [[NSDate date] addMinutes:minutesToAdd];
}

- (NSDate *)addMinutes:(int)minutesToAdd
{
    return [self dateByAddingTimeInterval:60 * minutesToAdd];
}

- (BOOL)isDue
{
    NSTimeInterval timeSince1970 = [self timeIntervalSince1970];
    NSTimeInterval nowSince1970  = [[NSDate date] timeIntervalSince1970];
    
    return timeSince1970 < nowSince1970;
}

- (BOOL)isLaterThan:(NSDate *)date
{
    return([self compare:date] == NSOrderedDescending);
}

@end
