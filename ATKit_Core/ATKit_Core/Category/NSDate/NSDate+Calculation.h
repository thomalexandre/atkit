//
//  NSDate+Calculation.h
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/4/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Calculation)

/// Add number of hours to the current date time.
///
/// @param hoursToAdd : Number of hours to add.
+ (NSDate *)addHoursToCurrentTime:(int)hoursToAdd;

/// Add number of hours to the date.
///
/// @param hoursToAdd : Number of hours to add.
- (NSDate *)addHours:(int)hoursToAdd;

/// Add number of days to the current date time.
///
/// @param daysToAdd : Number of days to add.
+ (NSDate *)addDaysToCurrentTime:(int)daysToAdd;

/// Add number of days to the date.
///
/// @param daysToAdd : Number of days to add.
- (NSDate *)addDays:(int)daysToAdd;

/// Add number of minutes to the current date time.
///
/// @param minutesToAdd : Number of minutes to add.
+ (NSDate *)addMinutessToCurrentTime:(int)minutesToAdd;

/// Add number of minutes to the date.
///
/// @param minutesToAdd : Number of minutes to add.
- (NSDate *)addMinutes:(int)minutesToAdd;

/// Returns YES if the given date is passed already, NO otherwise.
- (BOOL)isDue;

/// Returns YES of the current date is more recent than the passing date.
- (BOOL)isLaterThan:(NSDate *)date;

@end
