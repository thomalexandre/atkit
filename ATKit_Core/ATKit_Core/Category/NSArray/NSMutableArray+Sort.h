//
//  NSMutableArray+Sort.h
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/11/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Sort)

// Shuffle the current array.
- (void)shuffle;

@end
