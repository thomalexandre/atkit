//
//  NSString+Utils.h
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/23/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)

/// return true if the String is empty.
- (BOOL)empty;

@end
