//
//  NSString+Html.h
//  ATKit_Core
//
//  Created by Alexandre THOMAS on 29/09/15.
//  Copyright © 2015 athomas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Html)

- (NSString *)unescapeXml;
- (NSString *)cleanHtmlTags;

@end
