//
//  NSString+Html.h
//  ATKit_Core
//
//  Created by Alexandre THOMAS on 29/09/15.
//  Copyright © 2015 athomas. All rights reserved.
//

#import "NSString+Html.h"

@implementation NSString (Utilities)

- (NSString *)unescapeXml
{
    return [[[[[[[[self stringByReplacingOccurrencesOfString:@"&amp;"  withString:@"&"]
                        stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""]
                        stringByReplacingOccurrencesOfString:@"&#27;"  withString:@"'"]
                        stringByReplacingOccurrencesOfString:@"&#39;"  withString:@"'"]
                        stringByReplacingOccurrencesOfString:@"&#92;"  withString:@"'"]
                        stringByReplacingOccurrencesOfString:@"&#96;"  withString:@"'"]
                        stringByReplacingOccurrencesOfString:@"&gt;"   withString:@">"]
                        stringByReplacingOccurrencesOfString:@"&lt;"   withString:@"<"];
}


- (NSString *)cleanHtmlTags
{
    return [[[[[[self stringByReplacingOccurrencesOfString:@"<i>"   withString:@""]
                      stringByReplacingOccurrencesOfString:@"</i>"  withString:@""]
                      stringByReplacingOccurrencesOfString:@"<b>"   withString:@""]
                      stringByReplacingOccurrencesOfString:@"</b>"  withString:@""]
                      stringByReplacingOccurrencesOfString:@"<em>"  withString:@""]
                      stringByReplacingOccurrencesOfString:@"</em>" withString:@""];
    
}


@end
