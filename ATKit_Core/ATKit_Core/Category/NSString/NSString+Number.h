//
//  NSString+Number.h
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/7/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Number)

/// Returns true if the string contains only digits between [0, 9]
- (BOOL)isAllDigits;

@end
