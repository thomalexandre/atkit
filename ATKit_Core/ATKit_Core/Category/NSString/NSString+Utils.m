//
//  NSString+Utils.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/23/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "NSString+Utils.h"

@implementation NSString (Utils)

- (BOOL)empty
{
    return [self length] <= 0;
}

@end
