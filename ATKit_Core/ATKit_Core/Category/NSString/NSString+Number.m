//
//  NSString+Number.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/7/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "NSString+Number.h"

@implementation NSString (Number)


- (BOOL)isAllDigits
{
    if([@"" isEqualToString:self]) {
        return NO;
    }
    
    NSCharacterSet *nonNumbers = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    NSRange r = [self rangeOfCharacterFromSet:nonNumbers];
    return r.location == NSNotFound;
}

@end
