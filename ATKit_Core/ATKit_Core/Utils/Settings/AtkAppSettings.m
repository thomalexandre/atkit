//
//  AtkAppSettings.m
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/18/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "AtkAppSettings.h"


#import <UIKit/UIKit.h>

@interface AtkAppSettings ()

@property (strong, nonatomic) NSMutableDictionary *runtimeSettings;

@end

@implementation AtkAppSettings

#pragma mark init singleton

- (instancetype)init
{
    self = [super init];
    
    return self;
}

+ (instancetype)instance
{
    static AtkAppSettings *appSettings = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        appSettings = [[self alloc] init];
    });
    
    return appSettings;
}

#pragma mark settings

- (id)settingForKey:(NSString *)key
{
//    NSArray *bundles = [NSBundle allBundles];
//    NSBundle *mainBundle = [bundles lastObject];
    NSBundle *mainBundle = [NSBundle mainBundle];
    return [mainBundle objectForInfoDictionaryKey:key];
}

#pragma mark device environment

- (BOOL)isIphone
{
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone;
}

- (BOOL)isIpad
{
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
}

- (BOOL)isTV
{
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomTV;
}

- (NSInteger)iOSVersion
{
    return [[[UIDevice currentDevice] systemVersion] integerValue];
}

- (NSString *)appVersion
{
    return [self settingForKey:@"CFBundleShortVersionString"];
}

- (NSString *)appBuild
{
    return [self settingForKey:@"CFBundleVersion"];
}

#pragma mark runtime settings

- (void)setRuntimeObject:(id)object forKey:(NSString *)key
{
    if(!self.runtimeSettings) {
        self.runtimeSettings = [NSMutableDictionary new];
    }
    
    [self.runtimeSettings setObject:object forKey:key];
}

- (id)runtimeObjectForKey:(NSString *)key
{
    return [self.runtimeSettings objectForKey:key];
}

#pragma mark persistent setting (stored in NSUserDefault)

- (BOOL)setPersistentSetting:(id)setting forKey:(NSString *)key
{
    NSUserDefaults *cache = [NSUserDefaults standardUserDefaults];
    [cache setObject:setting forKey:key];
    return [cache synchronize];
}

- (id)persistentSettingForKey:(NSString *)key
{
    NSUserDefaults *cache = [NSUserDefaults standardUserDefaults];
    return [cache objectForKey:key];
}

- (id)persistentSettingForKey:(NSString *)key withDefault:(id)defaultSetting
{
    id res = [self persistentSettingForKey:key];
    return res ? res : defaultSetting;
}

- (BOOL)removePersistentSettingForKey:(NSString *)key
{
    NSUserDefaults *cache = [NSUserDefaults standardUserDefaults];
    [cache removeObjectForKey:key];
    return [cache synchronize];
}

#pragma mark persistent setting (stored in NSUserDefault for a given Group)

- (BOOL)setPersistentSetting:(id)setting forKey:(NSString *)key forGroup:(NSString *)groupName
{
    NSUserDefaults *cache = [[NSUserDefaults alloc] initWithSuiteName:groupName];
    [cache setObject:setting forKey:key];
    return [cache synchronize];
}

- (id)persistentSettingForKey:(NSString *)key forGroup:(NSString *)groupName
{
    NSUserDefaults *cache = [[NSUserDefaults alloc] initWithSuiteName:groupName];
    return [cache objectForKey:key];
}

- (id)persistentSettingForKey:(NSString *)key withDefault:(id)defaultSetting forGroup:(NSString *)groupName
{
    id res = [self persistentSettingForKey:key forGroup:groupName];
    return res ? res : defaultSetting;
}

- (BOOL)removePersistentSettingForKey:(NSString *)key forGroup:(NSString *)groupName
{
    NSUserDefaults *cache = [[NSUserDefaults alloc] initWithSuiteName:groupName];
    [cache removeObjectForKey:key];
    return [cache synchronize];
}


- (void)clearCookies
{
    // delete the cookies if any
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
