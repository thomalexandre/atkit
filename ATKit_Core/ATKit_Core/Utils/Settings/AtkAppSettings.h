//
//  AtkAppSettings.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/18/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AtkAppSettings : NSObject

+ (instancetype)instance;

#pragma mark settings

/// Returns the settings for the given key included into your info.plist.
///
/// @param key: the key of the setting.
- (id)settingForKey:(NSString *)key;

#pragma mark device environment

/// Returns true if the current device is an iPhone.
- (BOOL)isIphone;

/// Returns true if the current device is an iPad.
- (BOOL)isIpad;

/// Returns true if the current device is a TV.
- (BOOL)isTV;

/// Return the iOS version of the current device, 7, 8, 9 ...
- (NSInteger)iOSVersion;

/// Retuens the version of the application.
- (NSString *)appVersion;

/// Returns the build number of the application.
- (NSString *)appBuild;

#pragma mark runtime settings

/// Set a runtime object for the given key.
- (void)setRuntimeObject:(id)object forKey:(NSString *)key;

/// Return the given object for the given key.
- (id)runtimeObjectForKey:(NSString *)key;

#pragma mark persistent setting (stored in NSUserDefault)

/// Store a persistent setting for the given key.
/// Return YES if everything was fine
- (BOOL)setPersistentSetting:(id)setting forKey:(NSString *)key;

/// Ket the persistent setting for the given key
- (id)persistentSettingForKey:(NSString *)key;

/// Ket the persistent setting for the given key. Returns the defaultSetting if nothing is found
- (id)persistentSettingForKey:(NSString *)key withDefault:(id)defaultSetting;

/// Remove the persistent setting for the given key.
- (BOOL)removePersistentSettingForKey:(NSString *)key;

#pragma mark persistent setting (stored in NSUserDefault from a given group)

/// Store a persistent setting for the given key.
/// Return YES if everything was fine
- (BOOL)setPersistentSetting:(id)setting forKey:(NSString *)key forGroup:(NSString *)groupName;

/// Ket the persistent setting for the given key
- (id)persistentSettingForKey:(NSString *)key forGroup:(NSString *)groupName;

/// Ket the persistent setting for the given key. Returns the defaultSetting if nothing is found
- (id)persistentSettingForKey:(NSString *)key withDefault:(id)defaultSetting forGroup:(NSString *)groupName;

/// Remove the persistent setting for the given key.
- (BOOL)removePersistentSettingForKey:(NSString *)key forGroup:(NSString *)groupName;


/// Clear the persistent cookies.
- (void)clearCookies;

@end
