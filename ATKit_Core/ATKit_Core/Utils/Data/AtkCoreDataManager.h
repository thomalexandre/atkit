//
//  AtkCoreDataManager.h
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/5/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AtkApiClient;
@import CoreData;

typedef NS_ENUM(NSInteger, DataMode) {
    DataModeRead,   // context to read data
    DataModeWrite,  // context to write data
};

@interface AtkCoreDataManager : NSObject

@property (strong, nonatomic) AtkApiClient *atkApiClient;


/// SINGLETON instance access
+ (instancetype)instance;

#pragma mark context management

/// Create the object context from the persistent store.
/// Typically has to be called from the AppDelegate after didFinishLaunchingWithOptions
///
/// @param persistentStoreCoordinator : the coordinator to set up
- (void)createManagedObjectContextsWithPersistentStoreCoordinator:(NSPersistentStoreCoordinator *)persistentStoreCoordinator;

/// Save the insertion context after you make some modifications.
- (BOOL)saveInsertionContext;

#pragma mark Core Data access

/// Fetch one object from Core Data with the given parameters.
///
/// @param entityName : The Entity name.
/// @param predicate  : Predicate to use to search for entity.
/// @param moc        : The type of managed object context.
- (NSManagedObject *)fetchObjectEntityWithName:(NSString *)entityName
                                 withPredicate:(NSPredicate *)predicate
                                      withMode:(DataMode)mode;

/// Fetch one object from Core Data with the given parameters.
///
/// @param entityName      : The Entity name.
/// @param predicate       : Predicate to use to search for entity.
/// @param sortDescriptors : List of sorting descriptors to use to sort the objects.
/// @param moc             : The type of managed object context.
- (NSArray *)fetchObjectsEntityWithName:(NSString *)entityName
                          withPredicate:(NSPredicate *)predicate
                    withSortDescriptors:(NSArray *)sortDescriptors
                               withMode:(DataMode)mode;

/// Fetch one object from Core Data with the given parameters.
///
/// @param entityName      : The Entity name.
/// @param predicate       : Predicate to use to search for entity.
/// @param sortDescriptors : List of sorting descriptors to use to sort the objects.
/// @param moc             : The type of managed object context.
- (NSArray *)fetchObjectsEntityWithName:(NSString *)entityName
                          withPredicate:(NSPredicate *)predicate
                    withSortDescriptors:(NSArray *)sortDescriptors
                              withLimit:(NSUInteger)fetchLimit
                               withMode:(DataMode)mode;

/// Create a new object which will be returned from the insertion context.
///
/// @param entityName      : The Entity name.
- (NSManagedObject *)createEntityWithName:(NSString *)entityName;

/// Create a fetch Results controller to be used with Table view using the following parameters.
///
/// @param entityName         : name of the entity to fetch.
/// @param predicate          : predicate to filter entities.
/// @param sortDescriptors    : sorting of the entities.
/// @param sectionNameKeyPath : Grouping name.
- (NSFetchedResultsController *)fetchResultsControllerWithEntityName:(NSString *)entityName
                                                       withPredicate:(NSPredicate *)predicate
                                                 withSortDescriptors:(NSArray *)sortDescriptors
                                              withSectionNameKeyPath:(NSString *)sectionNameKeyPath;

/// Delete the given managed object.
- (void)deleteObject:(NSManagedObject *)object;

@end
