
//
//  AtkCoreDataManager.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/5/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "AtkCoreDataManager.h"
#import "AtkApiClient.h"

@interface AtkCoreDataManager ()

@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) NSManagedObjectContext *readingContext;
@property (strong, nonatomic) NSManagedObjectContext *insertionContext;

@end

@implementation AtkCoreDataManager

#pragma mark initialisation

- (id)init
{
    self = [super init];
    
    self.atkApiClient = [AtkApiClient new];
    
    return self;
}

+ (instancetype)instance
{
    static AtkCoreDataManager *coreDataManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        coreDataManager = [[self alloc] init];
    });
    
    return coreDataManager;
}

#pragma mark context management

- (void)createManagedObjectContextsWithPersistentStoreCoordinator:(NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    self.persistentStoreCoordinator = persistentStoreCoordinator;
    
    // create insertion context
    self.insertionContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    self.insertionContext.persistentStoreCoordinator = self.persistentStoreCoordinator;
    self.insertionContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
    
    // create reading context
    self.readingContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    self.readingContext.persistentStoreCoordinator = self.persistentStoreCoordinator;
    self.readingContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contextChanged:) name:NSManagedObjectContextDidSaveNotification object:nil];
}

- (void)contextChanged:(NSNotification*)notification
{
    NSManagedObjectContext *context = notification.object;
    if(context == self.readingContext)
        return;
    
    if(context.persistentStoreCoordinator != self.readingContext.persistentStoreCoordinator)
        return;
    
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(contextChanged:) withObject:notification waitUntilDone:YES];
        return;
    }
    
    [[self readingContext] mergeChangesFromContextDidSaveNotification:notification];
}

- (BOOL)saveInsertionContext
{
    @try {
        NSError *saveError = nil;
        return [self.insertionContext save:&saveError];
    }
    @catch (NSException * e) {
        NSLog(@"%@", e);
        return NO;
    }
}

#pragma mark Core Data access

- (NSFetchRequest *)createFetchRequestForEntityWithName:(NSString *)entityName
                                          withPredicate:(NSPredicate *)predicate
                                    withSortDescriptors:(NSArray *)sortDescriptors
                                              withLimit:(NSUInteger)fetchLimit
                                 inManagedObjectContext:(NSManagedObjectContext *)moc
{
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:moc];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    fetchRequest.entity = entity;
    fetchRequest.fetchLimit = fetchLimit;
    
    if (nil != predicate) {
        fetchRequest.predicate = predicate;
    }
    
    if (nil != sortDescriptors) {
        [fetchRequest setSortDescriptors:sortDescriptors];
    }
    
    return fetchRequest;
}

- (NSManagedObject *)fetchObjectEntityWithName:(NSString *)entityName
                                 withPredicate:(NSPredicate *)predicate
                                      withMode:(DataMode)mode
{
    NSArray *objects = [self fetchObjectsEntityWithName:entityName
                                          withPredicate:predicate
                                    withSortDescriptors:nil
                                            withMode:mode];
    
    return (0 != objects.count) ? objects[0] : nil;
}

- (NSArray *)fetchObjectsEntityWithName:(NSString *)entityName
                          withPredicate:(NSPredicate *)predicate
                    withSortDescriptors:(NSArray *)sortDescriptors
                               withMode:(DataMode)mode
{
    return [self fetchObjectsEntityWithName:entityName withPredicate:predicate withSortDescriptors:sortDescriptors withLimit:0 withMode:mode];
}

- (NSArray *)fetchObjectsEntityWithName:(NSString *)entityName
                          withPredicate:(NSPredicate *)predicate
                    withSortDescriptors:(NSArray *)sortDescriptors
                              withLimit:(NSUInteger)fetchLimit
                               withMode:(DataMode)mode
{
    @try {
        NSManagedObjectContext *managedObjectContext = [self mode:mode];
        NSFetchRequest *fetchRequest = [self createFetchRequestForEntityWithName:entityName
                                                                   withPredicate:predicate
                                                             withSortDescriptors:sortDescriptors
                                                                       withLimit:fetchLimit
                                                          inManagedObjectContext:managedObjectContext];
        NSError *error;
        NSArray *results = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
        if (nil != results) {
            return results;
        } else {
            NSLog(@"%@", [NSString stringWithFormat:@"%@ %@", error, error.localizedDescription]);
            return nil;
        }
    }
    @catch (NSException * e) {
        NSLog(@"%@", [NSString stringWithFormat:@"Exeption: %@", e]);
        NSLog(@"%@", [NSString stringWithFormat:@"Entity: %@ %@ %@", entityName, predicate, [NSThread description]]);
    }
    @finally {
        
    }
    
    return nil;
}

- (NSManagedObject *)createEntityWithName:(NSString *)entityName
{
    return [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:self.insertionContext];
}

- (NSManagedObjectContext *)mode:(DataMode)mode
{
    switch (mode) {
        case DataModeRead:   return self.readingContext;
        case DataModeWrite: return self.insertionContext;
    }
}

- (NSFetchedResultsController *)fetchResultsControllerWithEntityName:(NSString *)entityName
                                                       withPredicate:(NSPredicate *)predicate
                                                 withSortDescriptors:(NSArray *)sortDescriptors
                                              withSectionNameKeyPath:(NSString *)sectionNameKeyPath
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:self.readingContext];
    fetchRequest.entity = entity;
    fetchRequest.predicate = predicate;
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *controller = [[NSFetchedResultsController alloc]
                                              initWithFetchRequest:fetchRequest
                                              managedObjectContext:self.readingContext
                                              sectionNameKeyPath:sectionNameKeyPath
                                              cacheName:nil];
    
    return controller;
}

- (void)deleteObject:(NSManagedObject *)object
{
    [self.insertionContext deleteObject:object];
}

@end
