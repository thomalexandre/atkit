//
//  AtkImageLoader.h
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/1/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AtkImageLoader;

@protocol AtkImageLoaderDelegate <NSObject>

/// Delegate method called when the image has been downloaded from the network.
- (void)imageLoader:(AtkImageLoader *)loader imageHasBeenLoaded:(UIImage *)image;

@optional

/// Delegate method called when an error happened.
- (void)imageLoader:(AtkImageLoader *)loader errorLoadingImage:(NSError *)error;

/// Delegate method called during the download to get the current download progress of the image.
- (void)imageLoader:(AtkImageLoader *)loader progress:(CGFloat)progress withImageSize:(CGFloat)imageSize;

@end

@interface AtkImageLoader : NSObject

@property (strong, nonatomic) NSURL *url;

/// Initialize the loader with the given URL.
- (id)initWithURL:(NSURL *)url withDelegate:(id<AtkImageLoaderDelegate>)delegate;

/// Start downloading image. Use delegate to get call back when download is done.
- (void)loadImage;

@end
