//
//  AtkImageLoader.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/1/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "AtkImageLoader.h"
#import "AtkCrypto.h"

@interface AtkImageLoader () <NSURLSessionDelegate>

@property (strong, nonatomic) NSURLSession  *urlSession;
@property (strong, nonatomic) NSMutableData *dataToDownload;
@property (assign, nonatomic) CGFloat        imageSize;
@property (nonatomic, weak) id<AtkImageLoaderDelegate> delegate;

@end

@implementation AtkImageLoader

- (instancetype)initWithURL:(NSURL *)url withDelegate:(id<AtkImageLoaderDelegate>)delegate
{
    self = [super init];
    
    self.url = url;
    self.delegate = delegate;
    
    return self;
}

- (void)loadImage
{
    if(!self.url) {
        return;
    }
    
    NSData *dataFromCache = [self loadImageFromCacheWithURL:self.url];
    if(dataFromCache) {
        [self loadData:dataFromCache];
    }
    else {
        [self loadFromNetwork];
    }
}

- (void)loadFromNetwork
{
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.urlSession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask *dataTask = [self.urlSession dataTaskWithURL:self.url];
    [dataTask resume];
}

#pragma mark Cache

- (NSData *)loadImageFromCacheWithURL:(NSURL *)url
{
    NSString *filePath = [self getImageFilePathForCacheWithURL:url];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
        return [[NSFileManager defaultManager] contentsAtPath:filePath];
    }
    
    return nil;
}

- (NSString *)getImageFilePathForCacheWithURL:(NSURL *)url
{
    NSString *filename = [AtkCrypto encryptText:[url absoluteString]];
    NSString *encodedFileName = [[filename stringByReplacingOccurrencesOfString:@"/" withString:@"_"] stringByReplacingOccurrencesOfString:@":" withString:@"_"];
    return [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", encodedFileName]];
}

#pragma mark URL Session delegate

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler
{
    completionHandler(NSURLSessionResponseAllow);
    
    self.imageSize = [response expectedContentLength];
    self.dataToDownload = [[NSMutableData alloc]init];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data
{
    [self.dataToDownload appendData:data];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(imageLoader:progress:withImageSize:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            CGFloat progress = (CGFloat)([self.dataToDownload length]) / self.imageSize;
            [self.delegate imageLoader:self progress:progress withImageSize:self.imageSize];
        });
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if(error) {
        [self reportError:error];
        return;
    }
    
    if (self.dataToDownload != nil && [self.dataToDownload length] > 0) {

        [self loadData:self.dataToDownload];
        NSError *writeToFileError;
        NSString *pathToWrite = [self getImageFilePathForCacheWithURL:self.url];
        [self.dataToDownload writeToFile:pathToWrite options:NSDataWritingFileProtectionNone error:&writeToFileError];
    }
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error
{
    [self reportError:error];
}

#pragma mark Delegate call

- (void)reportError:(NSError *)error
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(imageLoader:errorLoadingImage:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate imageLoader:self errorLoadingImage:error];
        });
    }
}

- (void)loadData:(NSData *)imageData
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(imageLoader:imageHasBeenLoaded:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *image = [[UIImage alloc] initWithData:imageData];
            [self.delegate imageLoader:self imageHasBeenLoaded:image];
        });
    }
}

@end
