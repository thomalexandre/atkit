//
//  AtkApiClient.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/5/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "ATKApiClient.h"
#import "NSData+Convert.h"

#define kKeyDateOfLastCachedDataRequested @"dateOfLastCachedDataRequested"
#define kKeyCachedData                    @"cachedData"

typedef NS_ENUM(NSInteger, RequestMethod) {
    MethodGET,
    MethodPUT,
    MethodPOST,
    MethodDELETE,
};

@interface AtkApiClient ()

@property (strong, nonatomic) NSURLSession *urlSession;

@end

@implementation AtkApiClient

- (id)init
{
    self = [super init];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.urlSession  = [NSURLSession sessionWithConfiguration:config];
    self.logging = YES;
    
    return self;
}

- (void)getRequestWithURL:(NSURL *)url
               withHeader:(NSDictionary *)header
              withPayload:(NSDictionary *)payload
        withCacheDuration:(NSTimeInterval)cacheDuration
                onSuccess:(void (^)(NSData *data))success
                onFailure:(void (^)(NSError *error))failure
{
    [self requestWithURL:url withMethod:MethodGET withHeader:header withPayload:payload withCacheDuration:cacheDuration onSuccess:success onFailure:failure];
}

- (void)putRequestWithURL:(NSURL *)url
               withHeader:(NSDictionary *)header
              withPayload:(NSDictionary *)payload
                onSuccess:(void (^)(NSData *data))success
                onFailure:(void (^)(NSError *error))failure
{
    [self requestWithURL:url withMethod:MethodPUT withHeader:header withPayload:payload withCacheDuration:0 onSuccess:success onFailure:failure];
}

- (void)postRequestWithURL:(NSURL *)url
                withHeader:(NSDictionary *)header
               withPayload:(NSDictionary *)payload
                 onSuccess:(void (^)(NSData *data))success
                 onFailure:(void (^)(NSError *error))failure
{
    [self requestWithURL:url withMethod:MethodPOST withHeader:header withPayload:payload withCacheDuration:0 onSuccess:success onFailure:failure];
}

- (void)deleteRequestWithURL:(NSURL *)url
                  withHeader:(NSDictionary *)header
                 withPayload:(NSDictionary *)payload
                   onSuccess:(void (^)(NSData *data))success
                   onFailure:(void (^)(NSError *error))failure
{
    [self requestWithURL:url withMethod:MethodDELETE withHeader:header withPayload:payload withCacheDuration:0 onSuccess:success onFailure:failure];
}

- (void)requestWithURL:(NSURL *)url
            withMethod:(RequestMethod)method
            withHeader:(NSDictionary *)header
           withPayload:(NSDictionary *)payload
     withCacheDuration:(NSTimeInterval)cacheDuration
             onSuccess:(void (^)(NSData *data))success
             onFailure:(void (^)(NSError *error))failure
{
    // Check if we have something in the cache which matches this request, and returns it if we can
    NSData *dataFromCache = [self lookDataFromCacheWithURL:url withMethod:[self methodAsString:method] withCacheDuration:cacheDuration];
    if(dataFromCache) {
        if(success) {
            success(dataFromCache);
        }
        return;
    }
    
    // we check the API call
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:[self methodAsString:method]];
    
    if(header) {
        for(NSString *httpHeaderField in [header allKeys]) {
            NSString *value = [header valueForKey:httpHeaderField];
            [request setValue:value forHTTPHeaderField:httpHeaderField];
        }
    }
    
    if(payload) {
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&error];
        [request setHTTPBody:postData];
    }
    
    if(self.logging) {
        [self logRequestWithURL:url withHeader:header withPayload:payload];
    }
    
    [self executeRequest:request withCacheDuration:cacheDuration onSuccess:success onFailure:failure];
}

- (void)executeRequest:(NSURLRequest *)request
     withCacheDuration:(NSTimeInterval)cacheDuration
             onSuccess:(void (^)(NSData *data))success
             onFailure:(void (^)(NSError *error))failure
{
    NSDate *requestStart = [NSDate date];
    
    NSURLSessionDataTask *dataTask = [self.urlSession dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        
        NSDate *requestFinish = [NSDate date];
        NSTimeInterval executionTime = [requestFinish timeIntervalSinceDate:requestStart];
        NSLog(@"- Execution Time = %fs", executionTime);
        
        if(self.logging) {
            [self logResponseWithRequest:request withData:data withResponse:response withError:error];
        }
        
        NSError *errorFound = [self responseContainsError:error withResponse:response withData:data];
        if(errorFound) {
            if(failure) {
                failure(errorFound);
            }
        } else {
            if(cacheDuration > 0) {
                [self updateCacheWithURL:request.URL withMethod:request.HTTPMethod withData:data];
            }
            if(success) {
                success(data);
            }
        }
    }];
    
    [dataTask resume];
}

- (NSError *)responseContainsError:(NSError *)error
                      withResponse:(NSURLResponse *)response
                          withData:(NSData *)data
{
    if(error) {
        return error;
    }
    
    NSString *domain     = response.URL.host;
    NSInteger statusCode = ((NSHTTPURLResponse *)response).statusCode;
    if(statusCode != 200) {
        NSString *description = [NSHTTPURLResponse localizedStringForStatusCode:statusCode];
        return [NSError errorWithDomain:domain code:statusCode userInfo:@{NSLocalizedDescriptionKey:description}];
    }
    
    return nil;
}

- (void)logRequestWithURL:(NSURL *)url
               withHeader:(NSDictionary *)header
              withPayload:(NSDictionary *)payload
{
    NSLog(@"Request:----------");
    NSLog(@"- URL : %@", url);
    if(header) {
        NSLog(@"- Header : %@", header);
    }
    if(payload) {
         NSLog(@"- Payload : %@", payload);
    }
}

- (void)logResponseWithRequest:(NSURLRequest *)request
                      withData:(NSData *)data
                  withResponse:(NSURLResponse *)response
                     withError:(NSError *)error
{
    NSLog(@"Response:----------");
    NSLog(@"- URL : %@", request.URL);
    if(request.allHTTPHeaderFields && [request.allHTTPHeaderFields count]) {
        NSLog(@"- Header : %@", request.allHTTPHeaderFields);
    }
    if(error) {
        NSLog(@"- Error : %ld, %@", (long)error.code, error.localizedDescription);
    }
    
    if ([response isKindOfClass:[NSHTTPURLResponse class]])
    {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*) response;
        NSLog(@"- Response : %ld %@", (long)httpResponse.statusCode, [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]);
    }
    
    if(data) {
        NSLog(@"- Data (%ld)", (unsigned long)[data length]);
        NSLog(@"%@", [data isDictionary] ? [data jsonDictionary] : [data jsonArray]);
    } 
    NSLog(@"----------");
}

- (NSString *)methodAsString:(RequestMethod)method
{
    switch (method) {
        case MethodGET:     return @"GET";
        case MethodPUT:     return @"PUT";
        case MethodPOST:    return @"POST";
        case MethodDELETE:  return @"DELETE";
    }
}

#pragma mark - Cache Management

- (NSData *)lookDataFromCacheWithURL:(NSURL *)url
                          withMethod:(NSString *)method
                   withCacheDuration:(NSTimeInterval)cacheDuration
{
    NSData *result = nil;
    do {
        if(!url) {
            break;
        }
        
        if(cacheDuration == 0) {
            break;
        }
        
        NSUserDefaults *cache = [NSUserDefaults standardUserDefaults];
        NSString       *key   = [self keyWithURL:url withMethod:method];
        NSDictionary *dataFromCache = [cache objectForKey:key];
        if(!dataFromCache) {
            break;
        }
        
        NSDate *dateOfRequest = [dataFromCache valueForKey:kKeyDateOfLastCachedDataRequested];
        if(!dateOfRequest) {
            break;
        }
        
        NSDate *now = [NSDate new];
        NSTimeInterval timeSinceCachedData = [now timeIntervalSinceDate:dateOfRequest];
        if(ABS(timeSinceCachedData) > cacheDuration) {
            break;
        }
        
        result = [dataFromCache valueForKey:kKeyCachedData];
        NSLog(@"Data loaded from cache:----------");
        NSLog(@"%@ %@,", url, method);
        
    } while (false);
    
    return result;
}

- (void)updateCacheWithURL:(NSURL *)url
                withMethod:(NSString *)method
                  withData:(NSData *)data
{
    NSUserDefaults *cache      = [NSUserDefaults standardUserDefaults];
    NSDictionary *dataForCache = @{kKeyDateOfLastCachedDataRequested : [NSDate new],
                                   kKeyCachedData                    : data};
    NSString       *key       = [self keyWithURL:url withMethod:method];
    [cache setObject:dataForCache forKey:key];
    [cache synchronize];
    
    NSLog(@"Data saved to cache:----------");
    NSLog(@"%@ %@,", url, method);
}

- (NSString *)keyWithURL:(NSURL *)url withMethod:(NSString *)method
{
    return [NSString stringWithFormat:@"%@%@", method, url];
}

@end
