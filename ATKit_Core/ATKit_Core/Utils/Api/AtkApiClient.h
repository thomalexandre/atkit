//
//  AtkApiClient.h
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/5/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AtkApiClient : NSObject

/// Log url and the response after it arrives. Default is YES.
@property (nonatomic, assign) BOOL logging;

/// Initialize the API Client
- (id)init;

/// Run a GET request over the network with the given parameters. Execute the completionHandler when the code is done.
///
/// @param url           : url or the http query to execute.
/// @param header        : Dictionary containing the parameters to insert into the header of the request.
/// @param payload       : payload object to insert into the request.
/// @param cacheDuration : Duration to get data from the cache. If equalTo 0 we will always get from API call, and not store anything into cache.
///                        If last call happend in less than this interval, we get the data from cache.
///                        Please note the cached result will be only considered with the same URL only.
/// @param onSuccess     : completion handler will be executed when the request is done with success.
/// @param onFailure     : completion handler will be executed when the request is done with failure.
- (void)getRequestWithURL:(NSURL *)url
               withHeader:(NSDictionary *)header
              withPayload:(NSDictionary *)payload
        withCacheDuration:(NSTimeInterval)cacheDuration
                onSuccess:(void (^)(NSData *data))success
                onFailure:(void (^)(NSError *error))failure;

/// Run a PUT request over the network with the given parameters. Execute the completionHandler when the code is done.
///
/// @param url           : url or the http query to execute.
/// @param header        : Dictionary containing the parameters to insert into the header of the request.
/// @param payload       : payload object to insert into the request.
/// @param onSuccess     : completion handler will be executed when the request is done with success.
/// @param onFailure     : completion handler will be executed when the request is done with failure.
- (void)putRequestWithURL:(NSURL *)url
               withHeader:(NSDictionary *)header
              withPayload:(NSDictionary *)payload
                onSuccess:(void (^)(NSData *data))success
                onFailure:(void (^)(NSError *error))failure;

/// Run a POST request over the network with the given parameters. Execute the completionHandler when the code is done.
///
/// @param url           : url or the http query to execute.
/// @param header        : Dictionary containing the parameters to insert into the header of the request.
/// @param payload       : payload object to insert into the request.
/// @param onSuccess     : completion handler will be executed when the request is done with success.
/// @param onFailure     : completion handler will be executed when the request is done with failure.
- (void)postRequestWithURL:(NSURL *)url
                withHeader:(NSDictionary *)header
               withPayload:(NSDictionary *)payload
                 onSuccess:(void (^)(NSData *data))success
                 onFailure:(void (^)(NSError *error))failure;

/// Run a DELETE request over the network with the given parameters. Execute the completionHandler when the code is done.
///
/// @param url           : url or the http query to execute.
/// @param header        : Dictionary containing the parameters to insert into the header of the request.
/// @param payload       : payload object to insert into the request.
/// @param onSuccess     : completion handler will be executed when the request is done with success.
/// @param onFailure     : completion handler will be executed when the request is done with failure.
- (void)deleteRequestWithURL:(NSURL *)url
                  withHeader:(NSDictionary *)header
                 withPayload:(NSDictionary *)payload
                   onSuccess:(void (^)(NSData *data))success
                   onFailure:(void (^)(NSError *error))failure;

/// This method is called when the response comes back after the request to validate.
/// The method returns the error if we find one in the response
///
/// @param error    : error returned from the response.
/// @param response : the http response.
/// @NSError *aram data     : data of the response mapped as dictionary
- (NSError *)responseContainsError:(NSError *)error
                      withResponse:(NSURLResponse *)response
                          withData:(NSData *)data;

@end
