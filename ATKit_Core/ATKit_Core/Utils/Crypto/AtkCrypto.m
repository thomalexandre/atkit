//
//  AtkCrypto.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 11/02/16.
//  Copyright © 2016 athomas. All rights reserved.
//

#import "AtkCrypto.h"
#import "NSDate+Format.h"
#import <CommonCrypto/CommonHMAC.h>
#import <CommonCrypto/CommonCryptor.h>

@implementation AtkCrypto

+ (NSString *)encryptPassword:(NSString *)plainPassword withSalt:(NSString *)salt
{
    NSString *plainSalted   = [NSString stringWithFormat:@"%@{%@}", plainPassword, salt];
    NSData   *initialValue  = [plainSalted dataUsingEncoding:NSUTF8StringEncoding];
    
    unsigned char hash[CC_SHA512_DIGEST_LENGTH];
    CC_SHA512(initialValue.bytes, (unsigned int)initialValue.length, hash);

    for (int i = 1; i < 5000; i++)
    {
        
        NSMutableData * tmpData = [[NSMutableData alloc] initWithBytes:hash length:sizeof(hash)];
        [tmpData appendData:initialValue];
        CC_SHA512(tmpData.bytes, (unsigned int)tmpData.length, hash);
    }
    
    NSData *pwHashData = [[NSData alloc] initWithBytes:hash length: sizeof(hash)];
    return [pwHashData base64EncodedStringWithOptions:0];
}

+ (NSString *)authenticationHeaderWithUserName:(NSString *)userName withEncryptedassword:(NSString *)encryptedPassword
{
    NSData   *nonce                 = [AtkCrypto generateNonce];
    NSString *createTime            = [[NSDate date] universalFormat];
    NSData   *passwordAndTimeBuffer = [[NSString stringWithFormat:@"%@%@", createTime, encryptedPassword] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableData * diggestBuffer = [[NSMutableData alloc] initWithData:nonce];
    [diggestBuffer appendData:passwordAndTimeBuffer];
    
//    unsigned char digest[CC_SHA1_DIGEST_LENGTH];
//    CC_SHA1(diggestBuffer.bytes, (unsigned int)diggestBuffer.length, digest);
//    NSData *digestData = [[NSData alloc] initWithBytes:digest length:sizeof(digest)];
    
    NSData *digestData = [AtkCrypto hash:diggestBuffer withHashAlgorithm:HashAlgorithmSha1];
    NSString *digest64 = [digestData base64EncodedStringWithOptions:0];
    
    NSString * encryptedNonce = [nonce base64EncodedStringWithOptions:0];
    
    NSString *result = [NSString stringWithFormat:@"UsernameToken Username=\"%@\", PasswordDigest=\"%@\", Nonce=\"%@\", Created=\"%@\"", userName, digest64, encryptedNonce, createTime];
    
    return result;
}

+ (NSData *)generateNonce
{
    NSMutableData *data = [NSMutableData dataWithLength:16];
    int result = SecRandomCopyBytes(kSecRandomDefault, 16, data.mutableBytes);
    if (result != noErr) {
        return nil;
    }
    return data;
}

+ (NSData *)hash:(NSData *)data withHashAlgorithm:(HashAlgorithm)hashAlgorithm
{
    if(hashAlgorithm == HashAlgorithmSha512)
    {
        unsigned char hash[CC_SHA512_DIGEST_LENGTH];
        CC_SHA512(data.bytes, (unsigned int)data.length, hash);
        return [[NSData alloc] initWithBytes:hash length: sizeof(hash)];
    }
    else if(hashAlgorithm == HashAlgorithmSha256)
    {
        unsigned char hash[CC_SHA256_DIGEST_LENGTH];
        CC_SHA256(data.bytes, (unsigned int)data.length, hash);
        return [[NSData alloc] initWithBytes:hash length: sizeof(hash)];
    }
    else if(hashAlgorithm == HashAlgorithmSha1)
    {
        unsigned char hash[CC_SHA1_DIGEST_LENGTH];
        CC_SHA1(data.bytes, (unsigned int)data.length, hash);
        return [[NSData alloc] initWithBytes:hash length: sizeof(hash)];
    }
    
    return nil;
}

+ (NSString *)encryptText:(NSString *)text
{
    NSData *data      = [text dataUsingEncoding:NSUTF8StringEncoding];
    NSData *encrypted = [self hash:data withHashAlgorithm:HashAlgorithmSha1];
    return [encrypted base64EncodedStringWithOptions:0];
}

+ (NSString *)AES128PKCS7EncryptString:(NSString *)string withKey:(NSString *)key withIV:(NSData *)iv
{
    char keyPtr[kCCKeySizeAES128+1];
    bzero(keyPtr, sizeof(keyPtr));
    
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    
    NSUInteger dataLength = [string length];
    NSUInteger diff = kCCKeySizeAES128 - (dataLength % kCCKeySizeAES128);
    NSUInteger newSize = 0;
    
    if(diff > 0)
    {
        newSize = dataLength + diff;
    }
    
    size_t bufferSize = newSize + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    
    NSData *bytesString = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt,
                                          kCCAlgorithmAES128,
                                          kCCOptionPKCS7Padding, //No padding
                                          keyPtr,
                                          kCCKeySizeAES128,
                                          iv.bytes,
                                          bytesString.bytes,//dataPtr,
                                          bytesString.length,//sizeof(dataPtr),
                                          buffer,
                                          bufferSize,
                                          &numBytesEncrypted);
    
    if(cryptStatus == kCCSuccess)
    {
        NSData * result = [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
        return [result base64EncodedStringWithOptions:0];
    }
    
    return nil;
}
                            
@end
