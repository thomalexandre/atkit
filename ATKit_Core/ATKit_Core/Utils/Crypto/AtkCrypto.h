//
//  AtkCrypto.h
//  ATKit_Core
//
//  Created by Alexandre Thomas on 11/02/16.
//  Copyright © 2016 athomas. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, HashAlgorithm) {
    HashAlgorithmSha1,
    HashAlgorithmSha256,
    HashAlgorithmSha512,
};

@interface AtkCrypto : NSObject

/// Encrypt the given password together with the salt.
/// It uses SHA512 for encrypting.
/// Optimised for Symfony API rest call.
///
/// @param plainPassword : the plain password to encrypt.
/// @param salt          : salt to combine with the password for encryption.
+ (NSString *)encryptPassword:(NSString *)plainPassword withSalt:(NSString *)salt;

/// Create an authentication header with for API rest call on Symfony.
///
/// @param userName          : User's user name.
/// @param encryptedPassword : Prevously encrypted password (see method above).
+ (NSString *)authenticationHeaderWithUserName:(NSString *)userName withEncryptedassword:(NSString *)encryptedPassword;

/// Encode the given string with key and iv using AES 128 PKCS7 Encryption
+ (NSString *)AES128PKCS7EncryptString:(NSString *)string withKey:(NSString *)key withIV:(NSData *)iv;

/// Hash data with the given hash algorithm.
///
/// @param data          : The data to hash.
/// @parap hashAlgorithm : enum representing the algorithm to use for hashing.
+ (NSData *)hash:(NSData *)data withHashAlgorithm:(HashAlgorithm)hashAlgorithm;

/// Encrypt the given text with SHA1.
///
/// @parap text: text to encrypt.
+ (NSString *)encryptText:(NSString *)text;

/// Generate a random value
+ (NSData *)generateNonce;

@end
