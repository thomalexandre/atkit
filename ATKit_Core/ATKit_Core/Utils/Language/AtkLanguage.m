//
//  AtkLanguage.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/6/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "AtkLanguage.h"

@implementation AtkLanguage

+ (NSString *)text:(NSString *)key
{
    return [AtkLanguage text:key file:@"Localizable" bundle:[NSBundle mainBundle]];
}

+ (NSString *)text:(NSString *)key file:(NSString *)file
{
    return [AtkLanguage text:key file:file bundle:[NSBundle mainBundle]];
}

+ (NSString *)text:(NSString *)key file:(NSString *)file bundle:(NSBundle *)bundle
{
    return NSLocalizedStringFromTableInBundle(key, file, bundle, nil);
}

@end
