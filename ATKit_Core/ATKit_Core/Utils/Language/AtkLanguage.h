//
//  AtkLanguage.h
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/6/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AtkLanguage : NSObject

/// Return the text for the given key. Using the mainBundle and file called "Localizable"
+ (NSString *)text:(NSString *)key;

/// Return the text for the given key and file. Using the mainBundle.
+ (NSString *)text:(NSString *)key file:(NSString *)file;

/// Return the text for the given key, file and bundle.
+ (NSString *)text:(NSString *)key file:(NSString *)file bundle:(NSBundle *)bundle;

@end
