//
//  Logger.h
//  ATKit_Core
//
//  Created by Alexandre Thomas on 11/02/16.
//  Copyright © 2016 athomas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AtkLogger : NSObject

+ (void)debug:(NSString *)message;
+ (void)error:(NSString *)message;

@end
