//
//  AtkLogger.m
//  ATKit_Core
//
//  Created by Alexandre Thomas on 11/02/16.
//  Copyright © 2016 athomas. All rights reserved.
//

#import "AtkLogger.h"

@implementation AtkLogger

+ (NSString *)getPrefix
{
    NSString *sourceString = [[NSThread callStackSymbols] objectAtIndex:2];
    NSCharacterSet *separatorSet = [NSCharacterSet characterSetWithCharactersInString:@" -[]+?.,"];
    NSMutableArray *array = [NSMutableArray arrayWithArray:[sourceString componentsSeparatedByCharactersInSet:separatorSet]];
    [array removeObject:@""];
    
    //    NSLog(@"Stack = %@", [array objectAtIndex:0]);
    //    NSLog(@"Framework = %@", [array objectAtIndex:1]);
    //    NSLog(@"Memory address = %@", [array objectAtIndex:2]);
    //    NSLog(@"Class caller = %@", [array objectAtIndex:3]);
    //    NSLog(@"Function caller = %@", [array objectAtIndex:4]);
    
    return [NSString stringWithFormat:@"[%@:%@]", [array objectAtIndex:3], [array objectAtIndex:4]];
}

+ (void)debug:(NSString *)message
{
    NSLog(@"[DEBUG] %@ %@", [AtkLogger getPrefix], message);
}

+ (void)error:(NSString *)message
{
    NSLog(@"[ERROR] %@ %@", [AtkLogger getPrefix], message);
}

@end
