//
//  ATKit_Core.h
//  ATKit_Core
//
//  Created by Alexandre Thomas on 3/4/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ATKit_Core.
FOUNDATION_EXPORT double ATKit_CoreVersionNumber;

//! Project version string for ATKit_Core.
FOUNDATION_EXPORT const unsigned char ATKit_CoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ATKit_Core/PublicHeader.h>

// Category

#import <ATKit_Core/NSData+Convert.h>
#import <ATKit_Core/NSDate+Calculation.h>
#import <ATKit_Core/NSDate+Format.h>
#import <ATKit_Core/NSDictionary+DefaultValue.h>
#import <ATKit_Core/NSString+Html.h>
#import <ATKit_Core/NSString+Number.h>
#import <ATKit_Core/NSString+Utils.h>
#import <ATKit_Core/NSMutableArray+Sort.h>

// Utils

#import <ATKit_Core/AtkApiClient.h>
#import <ATKit_Core/AtkCoreDataManager.h>
#import <ATKit_Core/AtkCrypto.h>
#import <ATKit_Core/AtkReachability.h>
#import <ATKit_Core/AtkLanguage.h>
#import <ATKit_Core/AtkImageLoader.h>
#import <ATKit_Core/AtkAppSettings.h>