//
//  AtkWebViewController.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/16/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@import WebKit;

@interface AtkWebViewController : UIViewController  <UIWebViewDelegate>

@property (strong, nonatomic, nonnull) UIWebView *webView;

- (nonnull id)initWithURL:(nonnull NSURL *)url;

- (void)startLoadingActivity;
- (void)stopLoadingActivity;

#pragma mark Web View Delegate

- (void)webViewDidStartLoad:(nonnull UIWebView *)webView;
- (void)webViewDidFinishLoad:(nonnull UIWebView *)webView;
- (void)webView:(nonnull UIWebView *)webView didFailLoadWithError:(nullable NSError *)error;

@end
