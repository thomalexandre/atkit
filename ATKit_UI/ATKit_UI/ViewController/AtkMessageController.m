//
//  AtkMessageController.m
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/19/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "AtkMessageController.h"
#import "UIView+Layout.h"
#import "UIView+Utils.h"

@import ATKit_Core;

#define kMessageLabelHeight             64.0
#define kMessageLabelWidth              400.0
#define kAnimationDuration              0.75
#define kAnimationPause                 2
#define kKeyForDateWhenShowingMessage   @"kKeyForDateWhenShowingMessage"
#define kKeyDate                        @"date"
#define kKeyMessage                     @"message"
#define kButtonAnchorHeight             8
#define kButtonAnchorWidth              40
#define kButtonAnchorBottomSpace        4

#pragma mark Message Label View


@interface  AtkMessageLabel ()

@end

@implementation AtkMessageLabel

- (id)init
{
    self = [super init];
    
    AtkMessageLabel *appearance = [AtkMessageLabel appearance];
    
    // info
    self.infoBackgroundColor = appearance.infoBackgroundColor ? appearance.infoBackgroundColor : [UIColor grayColor];
    self.infoTextColor       = appearance.infoTextColor ? appearance.infoTextColor : [UIColor whiteColor];
    
    // success
    self.successBackgroundColor = appearance.successBackgroundColor ? appearance.successBackgroundColor : [UIColor greenColor];
    self.successTextColor       = appearance.successTextColor ? appearance.successTextColor : [UIColor blackColor];
    
    // alert
    self.alertBackgroundColor = appearance.alertBackgroundColor ? appearance.alertBackgroundColor :[UIColor redColor];
    self.alertTextColor       = appearance.alertTextColor ? appearance.alertTextColor : [UIColor whiteColor];
        
    return self;
}

- (void)setBackgroundColorWithStyle:(MessageStyle)style
{
    UIColor *color;
    switch (style) {
        case MessageStyleInfo:    color = self.infoBackgroundColor;    break;
        case MessageStyleSuccess: color = self.successBackgroundColor; break;
        case MessageStyleAlert:   color = self.alertBackgroundColor;   break;
    }
    
    self.backgroundColor = color;
}

- (void)setTextColorWithStyle:(MessageStyle)style
{
    UIColor *color;
    switch (style) {
        case MessageStyleInfo:    color = self.infoTextColor;    break;
        case MessageStyleSuccess: color = self.successTextColor; break;
        case MessageStyleAlert:   color = self.alertTextColor;   break;
    }
    
    self.textColor = color;
}

@end

#pragma mark Message Controller

static BOOL messageCurrentlyShown;

@interface AtkMessageController ()

@property (strong, nonatomic) UIView             *messageView;
@property (strong, nonatomic) NSLayoutConstraint *constraintToAnimate;
@property (assign, nonatomic) CGFloat             initialConstantValue;
@property (strong, nonatomic) NSString           *message;
@property (assign, nonatomic) MessageStyle        style;
@property (assign, nonatomic) NSTimeInterval      timeUntilNextMesage;
@property (assign, nonatomic) BOOL                weAreClosing;

@end

@implementation AtkMessageController

+ (AtkMessageController *)messageControllerWithMessage:(NSString *)message withStyle:(MessageStyle)style withTimeUntilNextMessage:(NSTimeInterval)timeUntilNextMessage
{
    AtkMessageController *messageController = [[AtkMessageController alloc] init];
    [messageController setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    messageController.message = message;
    messageController.style = style;
    messageController.timeUntilNextMesage = timeUntilNextMessage;
    
    return messageController;
}

+ (AtkMessageController *)messageControllerWithMessage:(NSString *)message withStyle:(MessageStyle)style
{
    return [self messageControllerWithMessage:message withStyle:style withTimeUntilNextMessage:-1];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupViews];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(![self doWeNeedToShowMessage]) {
        [self dismissViewControllerAnimated:NO completion:nil];
        return;
    }
    
    [self saveDateTimeOfMessageShown];
    [self animateMessageLabel];
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark UI setup

- (void)setupViews
{
    // Main view covering the status bar
    UIWindow *statusBarWindow = [[UIApplication sharedApplication] valueForKey:@"statusBarWindow"];
    self.messageView = [UIView new];
    [statusBarWindow addSubview:self.messageView];
    if([[AtkAppSettings instance] isIphone]) {
        [self autolayoutMessageLabelFromTop];
    } else {
        [self autolayoutMessageLabelFromLeft];
    }
    
    // Label
    AtkMessageLabel *messageLabel = [AtkMessageLabel new];
    messageLabel.text = self.message;
    [self.messageView addSubview:messageLabel];
    messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [messageLabel snapTop];
    [messageLabel snapRight];
    [messageLabel snapLeft];
    [messageLabel snapBottomConstant:kButtonAnchorBottomSpace + kButtonAnchorHeight];
    [messageLabel setBackgroundColorWithStyle:self.style];
    self.messageView.backgroundColor = messageLabel.backgroundColor;
    [messageLabel setTextColorWithStyle:self.style];
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.font = [AtkMessageLabel appearance].font ? [AtkMessageLabel appearance].font : [UIFont systemFontOfSize:16];
    messageLabel.numberOfLines = [AtkMessageLabel appearance].numberOfLines;
    
    // close anchor button
    if([[AtkAppSettings instance] isIphone]) {
        UIButton *button = [[UIButton alloc] init];
        [self.messageView addSubview:button];
        button.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:0.46];
        [button roundedCorner:kButtonAnchorHeight / 2];
        button.translatesAutoresizingMaskIntoConstraints = NO;
        [button snapBottomConstant:kButtonAnchorBottomSpace];
        [button centerX];
        [button setWidthConstant:kButtonAnchorWidth];
        [button setHeightConstant:kButtonAnchorHeight];
        [button addTarget:self action:@selector(userWantsToCloseTheMessage) forControlEvents:UIControlEventTouchUpInside];
        [button addTarget:self action:@selector(userWantsToCloseTheMessage) forControlEvents:UIControlEventTouchUpOutside];
    }
    
    // gesture recognizer to views to close the message if the user wants it
    [self addTapGestureRecognizerToView:self.messageView];
    [self addTapGestureRecognizerToView:self.view];
}

- (void)addTapGestureRecognizerToView:(UIView *)view
{
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userWantsToCloseTheMessage)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [view addGestureRecognizer:tapGestureRecognizer];
}

- (void)autolayoutMessageLabelFromTop
{
    self.messageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.constraintToAnimate = [self.messageView snapTopConstant:-kMessageLabelHeight];
    self.initialConstantValue = self.constraintToAnimate.constant;
    [self.messageView snapLeft];
    [self.messageView snapRight];
    [self.messageView setHeightConstant:kMessageLabelHeight];
}

- (void)autolayoutMessageLabelFromLeft
{
    self.messageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.messageView snapTop];
    self.constraintToAnimate = [self.messageView snapLeftConstant:-kMessageLabelWidth];
    self.initialConstantValue = self.constraintToAnimate.constant;
    [self.messageView setWidthConstant:kMessageLabelWidth];
    [self.messageView setHeightConstant:kMessageLabelHeight];
}

#pragma mark animations

- (void)animateMessageLabel
{
    messageCurrentlyShown = YES;
    
    [self.messageView layoutIfNeeded];
    [UIView animateWithDuration:kAnimationDuration delay:0 options:0 animations:^{
        self.constraintToAnimate.constant = 0;
        [self.messageView layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        NSTimeInterval delay = (kAnimationDuration + kAnimationPause);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self closeMessageViewWithAnimationDuration:kAnimationDuration];
        });
    }];
}

- (void)closeMessageViewWithAnimationDuration:(NSTimeInterval)duration
{
    if(self.weAreClosing) {
        return;
    }
    self.weAreClosing = YES;
    
    [UIView animateWithDuration:duration delay:0 options:0 animations:^{
        self.constraintToAnimate.constant = self.initialConstantValue;
        [self.messageView layoutIfNeeded];
    } completion:^(BOOL finished) {
        messageCurrentlyShown = NO;
        self.messageView.hidden = YES;
        [self dismissViewControllerAnimated:NO completion:^{
            [self.messageView removeFromSuperview];
        }];
    }];
}

- (void)userWantsToCloseTheMessage
{
    [self closeMessageViewWithAnimationDuration:(kAnimationDuration / 2)];
}

#pragma mark Timing management

- (NSDate *)lastTimeWeShowMessage
{
    NSString *key = [self keyForMessageAndStyle];
    NSDate *date = [[AtkAppSettings instance] runtimeObjectForKey:key];
    return date;
}

- (void)saveDateTimeOfMessageShown
{
    if(self.timeUntilNextMesage <= 0) // no need to save anything
        return;
    
    NSString *key = [self keyForMessageAndStyle];
    NSDate *currentDate = [NSDate date];
    [[AtkAppSettings instance] setRuntimeObject:currentDate forKey:key];
}

- (BOOL)doWeNeedToShowMessage
{
    if(messageCurrentlyShown) {
        return NO;
    }
    
    if(self.timeUntilNextMesage <= 0) {
        return YES;
    }
    
    NSDate *lastDate = [self lastTimeWeShowMessage];
    if(lastDate == nil) {
        return YES;
    }
    
    NSDate *currentDate = [NSDate date];
    NSTimeInterval interval = [currentDate timeIntervalSinceDate:lastDate];
    
    BOOL result = interval > self.timeUntilNextMesage;
    if(!result) {
        NSLog(@"We don't show the message right now, it was shown in less than %ld seconds (%f)", (long)self.timeUntilNextMesage, interval);
    }
    
    return result;
}

- (NSString *)keyForMessageAndStyle
{
    return [NSString stringWithFormat:@"%@_%ld_%@", kKeyForDateWhenShowingMessage, (long)self.style, [AtkCrypto encryptText:self.message]];
}

@end
