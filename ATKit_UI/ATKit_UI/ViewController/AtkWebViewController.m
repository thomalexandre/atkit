//
//  AtkWebViewController.m
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/16/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "AtkWebViewController.h"
#import "AtkActivityView.h"
#import "UIView+Layout.h"
#import "AtkMessageController.h"

@interface AtkWebViewController ()

@property (strong, nonatomic) NSURL     *url;
@property (strong, nonatomic) AtkActivityView *activityView;

@end

@implementation AtkWebViewController

- (id)initWithURL:(NSURL *)url
{
    self = [super init];
    
    self.url = url;
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupWebView];
    [self setupActivityIndicator];
}

#pragma mark instanciate UI

- (void)setupWebView
{
    UIWebView *webView = [[UIWebView alloc] init];
    [self.view addSubview:webView];
    [webView snap];
    self.webView = webView;
    [webView loadRequest:[NSURLRequest requestWithURL:self.url]];
    webView.delegate = self;
}

#pragma mark Wev View Delegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self startLoadingActivity];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self stopLoadingActivity];
    
    //usefull to disable horizontal scrolling
    [webView.scrollView setContentSize: CGSizeMake(webView.frame.size.width, webView.scrollView.contentSize.height)];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error
{
    [self stopLoadingActivity];
    
    AtkMessageController *messageController = [AtkMessageController messageControllerWithMessage:error.localizedDescription withStyle:MessageStyleAlert];
    [self presentViewController:messageController animated:YES completion:nil];
}

#pragma activity indicator

- (void)setupActivityIndicator
{
    self.activityView = [AtkActivityView activityViewToSuperView:self.view];
    
    [self.view bringSubviewToFront:self.activityView];
    [self startLoadingActivity];
}

- (void)startLoadingActivity
{
    self.activityView.hidden = NO;
}

- (void)stopLoadingActivity
{
    self.activityView.hidden = YES;
}

@end
