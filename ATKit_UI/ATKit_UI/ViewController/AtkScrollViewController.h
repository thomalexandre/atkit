//
//  AtkScrollViewController.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/22/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AtkScrollViewController : UIViewController

/// Overwrite to return your scrollview if needed.
- (UIScrollView *)scrollView;

@end
