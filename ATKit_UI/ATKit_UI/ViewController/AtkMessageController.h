//
//  AtkMessageController.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/19/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MessageStyle) {
    MessageStyleInfo,
    MessageStyleSuccess,
    MessageStyleAlert
};

@interface AtkMessageLabel : UILabel <UIAppearance>

@property (strong, nonatomic) UIColor *infoBackgroundColor      UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIColor *infoTextColor            UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIColor *successBackgroundColor   UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIColor *successTextColor         UI_APPEARANCE_SELECTOR;

@property (strong, nonatomic) UIColor *alertBackgroundColor     UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIColor *alertTextColor           UI_APPEARANCE_SELECTOR;

@end

@interface AtkMessageController : UIViewController

/// Example of configuration with Appearance Proxy
///   [[AtkMessageLabel appearance] setAlertBackgroundColor:[UIcolor blueColor]];
///   [[AtkMessageLabel appearance] setAlertTextColor:[UIcolor whiteColor]];
///   [[AtkMessageLabel appearance] setFont:[UIFont systemFontOfSize:18]];
///   [[AtkMessageLabel appearance] setNumberOfLines:0];
///
/// Run the message controller like this.
///   AtkMessageController *messageVC = [AtkMessageController messageControllerWithMessage:@"Error occurs" withStyle:MessageStyleAlert];
///   [self presentViewController:messageVC animated:NO completion:nil];
///
//////////////////////////////////////////////////////
///
/// Instanciate a message controller with a message and a style.
///
/// @param message : the text of the message.
/// @param style : the style of the message (usually change from success to alert to simple info)
+ (AtkMessageController *)messageControllerWithMessage:(NSString *)message withStyle:(MessageStyle)style;


/// Instanciate a message controller with a message, a style, and a timer. This will prevent of showing several time the same message in a row. We better wait a bit before presenting the message to the user on the next time.
///
/// @param message : the text of the message.
/// @param style : the style of the message (usually change from success to alert to simple info)
/// @param timeUntilNextMessage: If same message show recentyle, we will not show for now. If the value if <=0 we wont consider any timing and always show the message when triggered.
+ (AtkMessageController *)messageControllerWithMessage:(NSString *)message withStyle:(MessageStyle)style withTimeUntilNextMessage:(NSTimeInterval)timeUntilNextMessage;

@end
