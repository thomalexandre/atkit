//
//  AtkCollectionViewController.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 5/4/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AtkCollectionViewController : UIViewController

@property (strong, nonatomic) UICollectionView *collectionView;

/// setup the collection view. Overwrite and call super.
- (void)setupCollectionView;

@end
