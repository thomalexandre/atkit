//
//  AtkTableViewController.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 2/27/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AtkTableViewController : UIViewController

@property (strong, nonatomic) UITableView *tableView;

/// property to activate or not the refresh activiy
@property (assign, nonatomic) BOOL canRefresh __TVOS_PROHIBITED;

/// setup the table view. Overwrite and call super.
- (void)setupTableView;

/// Function called to load data for the table view.
- (void)reloadData;

/// Stop refreshing.
- (void)endRefreshing;

@end
