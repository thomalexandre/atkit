//
//  AtkScrollViewController.m
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/22/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "AtkScrollViewController.h"

@interface AtkScrollViewController ()

@end

@implementation AtkScrollViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [self listenToKeyboardNotifications];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark keyboard notifications

- (void)listenToKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    NSDictionary* info = [notification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    UIEdgeInsets insets = [self scrollView].contentInset;
    insets.bottom = keyboardSize.height;
    [self scrollView].contentInset = insets;
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    UIEdgeInsets insets = [self scrollView].contentInset;
    insets.bottom = 0;
    [self scrollView].contentInset = insets;
}

- (UIScrollView *)scrollView
{
    return nil;
}

@end
