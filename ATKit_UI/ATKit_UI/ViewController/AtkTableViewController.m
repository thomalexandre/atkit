//
//  AtkTableViewController.m
//  ATKit_UI
//
//  Created by Alexandre Thomas on 2/27/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "AtkTableViewController.h"
#import "UIView+Layout.h"

@interface AtkTableViewController ()

#if !TARGET_OS_TV
@property (nonatomic, strong) UIRefreshControl *refreshControl;
#endif

@end

@implementation AtkTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupTableView];
#if !TARGET_OS_TV
    if(self.canRefresh) {
        [self setupRefreshControl];
    }
#endif
}

- (void)setupTableView
{
    self.tableView = [UITableView new];
    [self.view addSubview:self.tableView];
    [self.tableView snap];
}

#if !TARGET_OS_TV
- (void)setupRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self
                            action:@selector(reloadData)
                  forControlEvents:UIControlEventValueChanged];
     [self.tableView addSubview:self.refreshControl];
}
#endif

- (void)reloadData
{
#if !TARGET_OS_TV
    [self.refreshControl beginRefreshing];
#endif
}

- (void)endRefreshing
{
#if !TARGET_OS_TV
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.refreshControl endRefreshing];
    });
#endif
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if([tableView respondsToSelector:@selector(setCellLayoutMarginsFollowReadableWidth:)]) {
        tableView.cellLayoutMarginsFollowReadableWidth = NO;
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}


@end
