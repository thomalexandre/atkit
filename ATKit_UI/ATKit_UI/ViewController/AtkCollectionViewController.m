//
//  AtkCollectionViewController.m
//  ATKit_UI
//
//  Created by Alexandre Thomas on 5/4/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "AtkCollectionViewController.h"
#import "UIView+Layout.h"

@interface AtkCollectionViewController ()

@end

@implementation AtkCollectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupCollectionView];
}

- (void)setupCollectionView
{
    UICollectionViewFlowLayout *flow = [UICollectionViewFlowLayout new];
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flow];
    [self.view addSubview:self.collectionView];
    [self.collectionView snap];
}



@end
