//
//  AtkUIManager.m
//  ATKit_UI
//
//  Created by Alexandre Thomas on 5/30/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "AtkUIManager.h"

@implementation AtkUIManager


+ (void)displayAllFontFamilyOnTheDevice
{
    for (NSString* family in [UIFont familyNames])
    {
        NSLog(@"%@", family);
        
        for (NSString* name in [UIFont fontNamesForFamilyName: family])
        {
            NSLog(@"  %@", name);
        }
    }
}

@end
