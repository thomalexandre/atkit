//
//  AtkUIManager.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 5/30/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface AtkUIManager : NSObject

// Display all font family to console available in the device
+ (void)displayAllFontFamilyOnTheDevice;

@end
