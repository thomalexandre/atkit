//
//  AtkGradientView.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/19/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "AtkGradientView.h"

@interface AtkGradientView ()

@property (nonatomic, strong) CAGradientLayer *gradient;

@end

@implementation AtkGradientView

- (id)init
{
    self = [super init];
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    
    self.gradient = [CAGradientLayer layer];
    self.gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:0 green:0 blue:0 alpha:0] CGColor],
                                                     (id)[[UIColor blackColor] CGColor], nil];
    [self.layer insertSublayer:self.gradient atIndex:0];
    
    [self setContentMode:UIViewContentModeRedraw];
    
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    self.gradient.frame = rect;
}

- (void)layoutIfNeeded
{
    [super layoutIfNeeded];
    self.gradient.frame = self.bounds;
    [self drawRect:self.bounds];
}

@end
