//
//  AtkGradientView.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/19/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AtkGradientView : UIView

- (id)init;

@end
