//
//  AtkActivityView.m
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/23/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "AtkActivityView.h"
#import "UIView+Layout.h"
#import "UIView+Utils.h"

@interface AtkActivityView ()

@property (strong, nonatomic) UIActivityIndicatorView *indicator;

@end

@implementation AtkActivityView

- (id)init
{
    self = [super init];
    
    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [self addSubview:self.indicator];
    [self.indicator setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.indicator centerX];
    [self.indicator centerY];
    
    return self;
}

- (void)setHidden:(BOOL)hidden
{
    [super setHidden:hidden];
    
    if(hidden)
       [self.indicator stopAnimating];
    else
        [self.indicator startAnimating];
}

+ (AtkActivityView *)activityViewToSuperView:(UIView *)superView
{
    AtkActivityView *activityView = [AtkActivityView new];
    activityView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [superView addSubview:activityView];
    
    [activityView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [activityView centerX];
    [activityView centerY];
    [activityView setHeightConstant:60];
    [activityView setWidthConstant:60];
    
    [activityView roundedCorner:8];
    
    
    return activityView;
}

@end
