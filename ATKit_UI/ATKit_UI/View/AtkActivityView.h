//
//  AtkActivityView.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/23/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AtkActivityView : UIView

- (void)setHidden:(BOOL)hidden;

/// Allocate a new activity View and set it up to the given super view
/// Size will be 60x60
+ (AtkActivityView *)activityViewToSuperView:(UIView *)superView;

@end
