//
//  AtkInnerShadowButton.m
//  ATKit_UI
//
//  Created by Alexandre Thomas on 14/11/15.
//  Copyright © 2015 Alexandre Thomas. All rights reserved.
//

#import "AtkInnerShadowButton.h"

@implementation AtkInnerShadowButton

- (void)awakeFromNib
{
    self.contentEdgeInsets = UIEdgeInsetsMake(0, 0, -5, 0);
    self.layer.shadowColor = self.innerShadowColor.CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 5.0);
    self.layer.shadowOpacity = 1.0;
    self.layer.shadowRadius = 0.0;
}


@end
