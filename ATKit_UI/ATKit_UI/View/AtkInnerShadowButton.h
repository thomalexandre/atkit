//
//  AtkInnerShadowButton.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 14/11/15.
//  Copyright © 2015 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface AtkInnerShadowButton : UIButton

// Color of the below shadow.
@property (copy, nonatomic) IBInspectable UIColor *innerShadowColor;

@end
