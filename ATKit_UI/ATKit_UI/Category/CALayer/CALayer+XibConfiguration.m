//
//  CALayer+XibConfiguration.m
//  ATKit_UI
//
//  Created by Alexandre Thomas on 14/11/15.
//  Copyright © 2015 Alexandre Thomas. All rights reserved.
//

#import "CALayer+XibConfiguration.h"

@implementation CALayer (XibConfiguration)

- (void)setBorderUIColor:(UIColor*)color
{
    self.borderColor = color.CGColor;
}

- (UIColor*)borderUIColor
{
    return [UIColor colorWithCGColor:self.borderColor];
}

@end