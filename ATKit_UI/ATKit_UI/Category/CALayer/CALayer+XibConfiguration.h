//
//  CALayer+XibConfiguration.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 14/11/15.
//  Copyright © 2015 Alexandre Thomas. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface CALayer (XibConfiguration)

/// This assigns a CGColor to borderColor.
@property(nonatomic, assign) UIColor* borderUIColor;

@end
