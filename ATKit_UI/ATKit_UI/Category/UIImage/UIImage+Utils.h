//
//  UIImage+Utils.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/29/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utils)

/// Resize the current image to the new size
- (UIImage *)resize:(CGSize)newSize;

@end
