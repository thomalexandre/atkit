//
//  UIView+Utils.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/17/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Utils)

/// Setup rounded corner for the given view
///
/// @param size: Size of the rounded corner.
- (void)roundedCorner:(CGFloat)size;

/// Create a border to the view with the given parameters.
///
/// @param color : color of the border.
/// @param size  : size of the border.
- (void)borderWithColor:(UIColor *)color withBorderWidth:(CGFloat)borderWidth;

// Returns the parent view controller of the current view.
- (UIViewController *)parentViewController;

/// Remove all subviews from the current view.
- (void)removeAllSubViews;

@end
