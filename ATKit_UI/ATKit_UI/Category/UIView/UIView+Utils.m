//
//  UIView+Utils.m
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/17/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "UIView+Utils.h"

@implementation UIView (Utils)

- (void)roundedCorner:(CGFloat)size
{
    self.clipsToBounds = YES;
    self.layer.cornerRadius = size;
}

- (void)borderWithColor:(UIColor *)color withBorderWidth:(CGFloat)borderWidth
{
    self.layer.borderColor = color.CGColor;
    self.layer.borderWidth = borderWidth;
}

- (UIViewController *)parentViewController
{
    UIResponder *responder = self;
    while ([responder isKindOfClass:[UIView class]]) {
        responder = [responder nextResponder];
    }
    
    return (UIViewController *)responder;
}

- (void)removeAllSubViews
{
    NSArray *viewsToRemove = [self subviews];
    for (UIView *view in viewsToRemove) {
        [view removeFromSuperview];
    }
}

@end
