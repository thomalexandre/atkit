//
//  UITextField+Text.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 24/11/15.
//  Copyright © 2015 Alexandre Thomas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UITextField (Text)

/// Returns the text of the text field trimmed
- (NSString *)trimText;

/// Returnes YES if the trimmed text of the text field is empty
- (BOOL)trimTextIsEmpty;

@end
