//
//  UITextField+Text.m
//  ATKit_UI
//
//  Created by Alexandre Thomas on 24/11/15.
//  Copyright © 2015 Alexandre Thomas. All rights reserved.
//

#import "UITextField+Text.h"

#if TARGET_OS_TV
@import ATKit_Core_tv;
#else
@import ATKit_Core;
#endif

@implementation UITextField (Text)


- (NSString *)trimText
{
    return [self.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (BOOL)trimTextIsEmpty
{
    NSString *trimText = [self trimText];
    return [trimText empty];
}

@end
