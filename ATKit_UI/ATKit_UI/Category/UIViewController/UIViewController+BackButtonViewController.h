//
//  UIViewController+BackButtonViewController.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/31/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (BackButtonViewController)

/// Replace the back button of the linked navigation controller with the given image from name.
- (void)setUpImageBackButtonWithName:(NSString *)imageName;

/// Replace the back button of the linked navigation controller with the given image.
- (void)setUpImageBackButtonWithImage:(UIImage *)image;

@end
