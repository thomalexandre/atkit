//
//  UIViewController+BackButtonViewController.m
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/31/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "UIViewController+BackButtonViewController.h"


@implementation UIViewController(BackButtonViewController)

- (void)setUpImageBackButtonWithName:(NSString *)imageName
{
    return [self setUpImageBackButtonWithImage:[UIImage imageNamed:imageName]];
}

- (void)setUpImageBackButtonWithImage:(UIImage *)image
{
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setBackgroundImage:image forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame = CGRectMake(0, 0, 22, 22);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
    self.navigationItem.leftBarButtonItem = backButton;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
}

- (void)goback
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
