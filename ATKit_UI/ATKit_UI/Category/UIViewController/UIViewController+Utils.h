//
//  UIViewController+Utils.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/11/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Utils)

/// Returns YES if the UIViewController is visible on the screen.
- (BOOL)isVisible;

/// Add the childView into the view and assign Autolayout full screen.
+ (void)snapChildView:(UIView *)childView toView:(UIView *)view;

/// Add content view controller as a sub view controller. view must be in the hierarchy.
- (void)addContentController:(UIViewController *)content toView:(UIView *)view;

// Same as above and add margin.
- (void)addContentController:(UIViewController *)content toView:(UIView *)view
               withTopMargin:(float)top
             withRightMargin:(float)right
            withBottomMargin:(float)bottom
              withleftMargin:(float)left;

/// Add content view controller as a sub view controller.
- (void)addContentController:(UIViewController *)content;

// Remove the content view controller from the current view controller.
- (void)removeContentController:(UIViewController *)content;

@end
