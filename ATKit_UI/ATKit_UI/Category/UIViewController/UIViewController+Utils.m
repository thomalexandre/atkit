//
//  UIViewController+Utils.m
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/11/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "UIViewController+Utils.h"
#import "UIView+Layout.h"

@implementation UIViewController (Utils)

- (BOOL)isVisible
{
    return [self isViewLoaded] && self.view.window;
}

+ (void)snapChildView:(UIView *)childView toView:(UIView *)view
{
    [view addSubview:childView];
    childView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [view addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                     attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:view
                                                     attribute:NSLayoutAttributeTop
                                                    multiplier:1.0
                                                      constant:0.0]];
    
    [view addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                     attribute:NSLayoutAttributeLeading
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:view
                                                     attribute:NSLayoutAttributeLeading
                                                    multiplier:1.0
                                                      constant:0.0]];
    
    [view addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                     attribute:NSLayoutAttributeTrailing
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:view
                                                     attribute:NSLayoutAttributeTrailing
                                                    multiplier:1.0
                                                      constant:0.0]];
    
    [view addConstraint:[NSLayoutConstraint constraintWithItem:childView
                                                     attribute:NSLayoutAttributeBottom
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:view
                                                     attribute:NSLayoutAttributeBottom
                                                    multiplier:1.0
                                                      constant:0.0]];
}

- (void)addContentController:(UIViewController *)content toView:(UIView *)view
{    
    [self addContentController:content toView:view withTopMargin:0 withRightMargin:0 withBottomMargin:0 withleftMargin:0];
}

- (void)addContentController:(UIViewController *)content toView:(UIView *)view withTopMargin:(float)top withRightMargin:(float)right withBottomMargin:(float)bottom withleftMargin:(float)left
{
    [self addChildViewController:content];
    [view addSubview:content.view];
    [content didMoveToParentViewController:self];
    
    [content.view setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [content.view snapTopConstant:top];
    [content.view snapRightConstant:right];
    [content.view snapLeftConstant:left];
    [content.view snapBottomConstant:bottom];
}

- (void)addContentController:(UIViewController*)content
{
    [self addContentController:content toView:self.view];
}

- (void)removeContentController:(UIViewController*)content
{
    [content.view removeFromSuperview];
    [content removeFromParentViewController];
}


@end
