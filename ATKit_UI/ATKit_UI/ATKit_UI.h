//
//  ATKit_UI.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/4/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ATKit_UI.
FOUNDATION_EXPORT double ATKit_UIVersionNumber;

//! Project version string for ATKit_UI.
FOUNDATION_EXPORT const unsigned char ATKit_UIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ATKit_UI/PublicHeader.h>


// Category
#import <ATKit_UI/CALayer+XibConfiguration.h>
#import <ATKit_UI/UITextField+Text.h>
#import <ATKit_UI/UIImage+Utils.h>
#import <ATKit_UI/UIView+Layout.h>
#import <ATKit_UI/UIView+Utils.h>
#import <ATKit_UI/UIViewController+Utils.h>
#import <AtKit_UI/UIViewController+BackButtonViewController.h>

/// Utils
#import <ATKit_UI/AtkUIManager.h>

// Views
#import <ATKit_UI/AtkGradientView.h>
#import <ATKit_UI/AtkActivityView.h>
#import <ATKit_UI/AtkInnerShadowButton.h>

// View controllers
#import <ATKit_UI/AtkCollectionViewController.h>
#import <ATKit_UI/AtkMessageController.h>
#import <ATKit_UI/AtkTableViewController.h>
#import <ATKit_UI/AtkScrollViewController.h>
#import <ATKit_UI/AtkWebViewController.h>