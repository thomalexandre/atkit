//
//  AtkMessageController.h
//  ATKit_UI
//
//  Created by Alexandre Thomas on 5/4/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AtkMessageController : UIAlertController

+ (AtkMessageController *)messageControllerWithMessage:(NSString *)message;

@end
