//
//  AtkMessageController.m
//  ATKit_UI
//
//  Created by Alexandre Thomas on 5/4/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import "AtkMessageController.h"

@interface AtkMessageController ()

@end

@implementation AtkMessageController


+ (AtkMessageController *)messageControllerWithMessage:(NSString *)message
{
    AtkMessageController * alertController = [AtkMessageController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    
    }];
    
    [alertController addAction:action];

    return alertController;
}

@end
