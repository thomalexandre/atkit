//
//  AtkAppSettingsTests.m
//  ATKit_UI
//
//  Created by Alexandre Thomas on 3/19/16.
//  Copyright © 2016 Alexandre Thomas. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AtkAppSettings.h"

@interface AtkAppSettingsTests : XCTestCase

@end

@implementation AtkAppSettingsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSettingForKey
{
    NSString *settingString = [[AtkAppSettings instance] settingForKey:@"AUTO_TEST_STRING"];
    XCTAssert([settingString isEqualToString:@"my test result"]);
    
    NSNumber *settingNumber = [[AtkAppSettings instance] settingForKey:@"AUTO_TEST_NUMBER"];
    XCTAssert([settingNumber isEqualToNumber:@100]);
}

@end
